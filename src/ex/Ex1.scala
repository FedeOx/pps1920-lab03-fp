package ex

object Ex1 {
  // A generic linkedlist
  sealed trait List[E]
  object List {
    case class Cons[E](head: E, tail: List[E]) extends List[E]
    case class Nil[E]() extends List[E]

    def sum(l: List[Int]): Int = l match {
      case Cons(h, t) => h + sum(t)
      case _ => 0
    }

    def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
      case (Cons(h, t), l2) => Cons(h, append(t, l2))
      case _ => l2
    }

    def map[A,B](l: List[A])(mapper: A=>B): List[B] =
      flatMap(l)(v => Cons(mapper(v), Nil()))

    def filter[A](l: List[A])(pred: A=>Boolean): List[A] = {
      flatMap(l)(v => pred(v) match {
        case true => Cons(v, Nil())
        case false => Nil()
      })
    }

    @scala.annotation.tailrec
    def drop[A](l: List[A], n: Int): List[A] = l match {
      case Cons(_, t) if n>0 => drop(t, n - 1)
      case Cons(h, t) if n==0 => Cons(h, t)
      case _ => Nil()
    }

    def flatMap[A,B](l: List[A])(f: A => List[B]): List[B] = l match {
      case Cons(h, t) => append(f(h), flatMap(t)(f))
      case _ => Nil()
    }

  }
}
