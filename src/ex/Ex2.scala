package ex

import Ex1.List
import Ex1.List._
import u02.Optionals.Option
import u02.Optionals.Option._

object Ex2 {

  def max(l: List[Int]): Option[Int] = l match {
    case Cons(h, y) if h >= getOrElse(max(y), -1) => Some(h)
    case Cons(h, y) if h < getOrElse(max(y), -1) => Some(getOrElse(max(y), -1))
    case _ => None()
  }

}
