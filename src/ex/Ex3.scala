package ex

import ex.Ex1.List
import ex.Ex1.List._

object Ex3 {

  sealed trait Person
  object Person {
    case class Student(name: String, year: Int) extends Person
    case class Teacher(name: String, course: String) extends Person

    def name(p: Person): String = p match {
      case Student(n, _) => n
      case Teacher(n, _) => n
    }
  }

  import Person._

  def fun(l: List[Person]): List[String] =
    map(filter(l)(p => p match {
      case Teacher(_,_) => true
      case _ => false
    }))(p => p match {
      case Teacher(_, c) => c
    })
}
