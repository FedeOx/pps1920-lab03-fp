package ex

import ex.Ex5.Stream
import ex.Ex5.Stream._

object Ex6 {

  def constant[A](k: A): Stream[A] =
    cons(k, constant(k))

}
