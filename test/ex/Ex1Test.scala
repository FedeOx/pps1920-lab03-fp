package ex

import ex.Ex1.List._
import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class Ex1Test {

  @Test def testDrop(): Unit = {
    val lst = Cons(10, Cons(20, Cons(30, Nil())))
    assertEquals(drop(lst, 1), Cons(20, Cons(30, Nil())))
    assertEquals(drop(lst, 2), Cons(30, Nil()))
    assertEquals(drop(lst, 5), Nil())
  }

  @Test def testDropWithZeroOrNegative(): Unit = {
    val lst = Cons(10, Cons(20, Cons(30, Nil())))
    assertEquals(drop(lst, 0), Cons(10, Cons(20, Cons(30, Nil()))))
    assertEquals(drop(lst, -1), Nil())
  }

  @Test def testDropWithEmptyList(): Unit = {
    assertEquals((drop(Nil(), 2)), Nil())
  }

  @Test def testFlatMap(): Unit = {
    val lst = Cons(10, Cons(20, Cons(30, Nil())))
    assertEquals(flatMap(lst)(v => Cons(v+1, Nil())), Cons(11, Cons(21, Cons(31, Nil()))))
    assertEquals(flatMap(lst)(v => Cons(v+1, Cons(v+2, Nil()))),
      Cons(11, Cons(12, Cons(21, Cons(22, Cons(31, Cons(32, Nil())))))))
  }

  @Test def testMap(): Unit = {
    val lst = Cons(10, Cons(20, Cons(30, Nil())))
    assertEquals(map(lst)(v => v+1), Cons(11, Cons(21, Cons(31, Nil()))))
  }

  @Test def testFilter(): Unit = {
    val lst = Cons(10, Cons(20, Cons(30, Nil())))
    assertEquals(filter(lst)(v => v>20), Cons(30, Nil()))

    val lst2 = Cons(10, Cons(20, Cons(30, Nil())))
    assertEquals(filter(lst2)(v => v>30), Nil())
  }

}
