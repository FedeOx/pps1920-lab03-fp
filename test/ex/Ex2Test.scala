package ex

import ex.Ex1.List._
import ex.Ex2._
import u02.Optionals.Option._
import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class Ex2Test {

  @Test def testMax(): Unit = {
    assertEquals(max(Cons(10, Cons(25, Cons(20, Nil())))), Some(25))
    assertEquals(max(Nil()), None())
  }

}
