package ex

import ex.Ex1.List
import ex.Ex1.List._
import ex.Ex3._
import ex.Ex3.Person._
import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class Ex3Test {

  @Test def testFun(): Unit = {
    val lst : List[Person] = Cons(Teacher("Luca", "Italiano"), Cons(Student("Carlo", 1), Cons(Teacher("Mario", "Informatica"),
      Cons(Student("Giorgio", 2), Nil()))))
    assertEquals(fun(lst), Cons("Italiano", Cons("Informatica", Nil())))
  }

}
