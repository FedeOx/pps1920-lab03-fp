package ex

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import Ex5.Stream
import Ex5.Stream._

class Ex5Test {

  @Test def testDrop(): Unit = {
    val s = Stream.take(Stream.iterate(0)(_+1))(10)
    assertEquals(Stream.toList(Stream.drop(s)(6)), Stream.toList(cons(6, cons(7, cons(8, cons(9, empty()))))))
  }

}
