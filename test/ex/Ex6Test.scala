package ex

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import ex.Ex5.Stream
import ex.Ex5.Stream._
import ex.Ex6._

class Ex6Test {

  @Test def testConstant(): Unit = {
    assertEquals(Stream.toList(Stream.take(constant("x"))(5)),
      Stream.toList(cons("x", cons("x", cons("x", cons("x", cons("x", empty())))))))

    assertEquals(Stream.toList(Stream.take(constant(3))(5)),
      Stream.toList(cons(3, cons(3, cons(3, cons(3, cons(3, empty())))))))
  }

}
